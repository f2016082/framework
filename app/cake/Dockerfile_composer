FROM alpine:3.7

ARG version
ARG apk_repo
ARG php_version

LABEL description="Alpine-based php-fpm image for CDLI Cake"
LABEL maintainer="Cuneiform Digital Library Initiative <cdli.systems@gmail.com>"
LABEL version="$version"

ENV PHP_VERSION=$php_version
ADD https://dl.bintray.com/php-alpine/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

RUN set -ex; \
  addgroup -g 2354 -S cdli; \
  adduser -u 2354 -D -S -G cdli cdli; \
  echo "@php $apk_repo" >> /etc/apk/repositories; \
  apk add --update \
    ca-certificates \
    git \
    php7 \
    php7-fpm \
    php7-ctype \
		php7-intl \
		php7-json \
    php7-dom \
    php7-mbstring \
    php7-mysqli \
    php7-mysqlnd \
    php7-opcache \
    php7-pdo \
    php7-pdo_mysql \
    php7-session \
    php7-xml \
    curl \
    php7-simplexml \
    php7-tokenizer \
    php7-xmlwriter \
    php7-phar \
    php7-apcu \
    nodejs \
    ruby-json \
    ruby-dev \
    ruby \
    nodejs-npm; \
  \
  rm -rf /var/cache/apk/* /tmp/pear ~/.pearrc;

# Installing python3 for python unittests.
RUN apk add --no-cache --virtual python3-dev && \
  apk add --no-cache --update python3 && \
  pip3 install --upgrade pip setuptools;

# Installing scss-lint.
RUN npm install -g csslint; \
  gem update --system; \
  gem install rdoc --no-document; \
  gem install scss-lint;

# Sane defaults
RUN set -ex; \
  sed -ri \
    -e 's|^;?expose_php = .*|expose_php = off|' \
    -e 's|^;?allow_url_fopen = .*|allow_url_fopen = on|' \
    -e 's|^;?opcache.error_log=.*|opcache.error_log = /proc/self/fd/2|' \
    -e 's|^;?opcache.fast_shutdown=.*|opcache.fast_shutdown = 1|' \
    /etc/php7/php.ini; 
  # sed -ri \
  #   -e 's|^;?error_log = .*|error_log = /proc/self/fd/2|' \
  #   -e 's|^;?emergency_restart_threshold = .*|emergency_restart_threshold = 4|' \
  #   -e 's|^;emergency_restart_interval = .*|emergency_restart_interval = 1m|' \
  #   -e 's|^;?process_control_timeout = .*|process_control_timeout = 10s|' \
  #   -e 's|^;?daemonize = .*|daemonize = no|' \
  #   -e 's|^;?events.mechanism = .*|events.mechanism = epoll|' \
  #   /etc/php7/php-fpm.conf; \
  # \
#  sed -ri \
#    -e 's|^;?user = .*|user = cdli|' \
#    -e 's|^;?group = .*|group = cdli|' \
#    -e 's|^;?listen = .*|listen = 9000|' \
#    -e 's|^;?pm = .*|pm = dynamic|' \
#    -e 's|^;?pm.max_children = .*|pm.max_children = 24|' \
#    -e 's|^;?pm.start_servers = .*|pm.start_servers = 8|' \
#    -e 's|^;?pm.min_spare_servers = .*|pm.min_spare_servers = 4|' \
#    -e 's|^;?pm.max_spare_servers = .*|pm.max_spare_servers = 12|' \
#    -e 's|^;?pm.max_requests = .*|pm.max_requests = 250|' \
#    -e 's|^;?pm.status_path = .*|pm.status_path = /status|' \
#    -e 's|^;?access.log = .*|access.log = /proc/self/fd/2|' \
#    -e 's|^;?access.format = .*|access.format = "%R - %u %t \"%m %r%Q%q\" %s"|' \
#    -e 's|^;?catch_workers_output = .*|catch_workers_output = yes|' \
#    /etc/php7/php-fpm.d/www.conf;

# Installing PHPlint
RUN curl -L https://cs.symfony.com/download/php-cs-fixer-v2.phar -o php-cs-fixer;
RUN chmod a+x php-cs-fixer;
RUN mv php-cs-fixer /usr/local/bin/php-cs-fixer;rm -f php-cs-fixer;

# Installing composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');";
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer;
RUN rm -f composer-setup.php;

# Adding path for cake folder
ENV PATH=${PATH}:/srv/app/cake/bin:/srv/app/cake/vendor/bin
# ENTRYPOINT [ "/bin/sh" ]