FROM alpine:3.11
ARG version
ARG apk_repo
ARG php_version

LABEL description="Alpine-based php-fpm image for CDLI Cake"
LABEL maintainer="Cuneiform Digital Library Initiative <cdli.systems@gmail.com>"
LABEL version="$version"

ENV PHP_VERSION=$php_version
ADD https://dl.bintray.com/php-alpine/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

RUN set -ex; \
  addgroup -g 2354 -S cdli; \
  adduser -u 2354 -D -S -G cdli cdli; \
  echo "https://dl.bintray.com/php-alpine/v3.11/php-7.4" >> /etc/apk/repositories;

RUN apk update;

RUN apk add --update \
  ca-certificates \
  php-fpm \
  php-ctype \
  php-intl \
  php-json \
  php-mbstring \
  php-mysqli \
  php-mysqlnd \
  php-opcache \
  php-pdo \
  php-pdo_mysql \
  php-session \
  php-xml \
  php-curl \
  php-redis \
  php-apcu; \
  \
  rm -rf /var/cache/apk/* /tmp/pear ~/.pearrc;
RUN apk add --update curl libcurl;
# Sane defaults


RUN set -ex; \
  sed -ri \
    -e 's|^;?expose_php = .*|expose_php = off|' \
    -e 's|^;?allow_url_fopen = .*|allow_url_fopen = off|' \
    -e 's|^;?opcache.error_log=.*|opcache.error_log = /proc/self/fd/2|' \
    -e 's|^;?opcache.fast_shutdown=.*|opcache.fast_shutdown = 1|' \
    -e 's|^;?upload_max_filesize =.*|upload_max_filesize = 20M|' \
    -e 's|^;?post_max_size =.*|post_max_size = 20M|' \
    /etc/php7/php.ini; \
  \
  sed -ri \
    -e 's|^;?error_log = .*|error_log = /proc/self/fd/2|' \
    -e 's|^;?emergency_restart_threshold = .*|emergency_restart_threshold = 4|' \
    -e 's|^;emergency_restart_interval = .*|emergency_restart_interval = 1m|' \
    -e 's|^;?process_control_timeout = .*|process_control_timeout = 10s|' \
    -e 's|^;?daemonize = .*|daemonize = no|' \
    -e 's|^;?events.mechanism = .*|events.mechanism = epoll|' \
    /etc/php7/php-fpm.conf; \
  \
  sed -ri \
    -e 's|^;?user = .*|user = cdli|' \
    -e 's|^;?group = .*|group = cdli|' \
    -e 's|^;?listen = .*|listen = 9000|' \
    -e 's|^;?pm = .*|pm = dynamic|' \
    -e 's|^;?pm.max_children = .*|pm.max_children = 24|' \
    -e 's|^;?pm.start_servers = .*|pm.start_servers = 8|' \
    -e 's|^;?pm.min_spare_servers = .*|pm.min_spare_servers = 4|' \
    -e 's|^;?pm.max_spare_servers = .*|pm.max_spare_servers = 12|' \
    -e 's|^;?pm.max_requests = .*|pm.max_requests = 250|' \
    -e 's|^;?pm.status_path = .*|pm.status_path = /status|' \
    -e 's|^;?access.log = .*|access.log = /proc/self/fd/2|' \
    -e 's|^;?access.format = .*|access.format = "%R - %u %t \"%m %r%Q%q\" %s"|' \
    -e 's|^;?catch_workers_output = .*|catch_workers_output = yes|' \
    /etc/php7/php-fpm.d/www.conf;

# RUN echo "extension=redis.so" >> /etc/php7/php.ini;

RUN mkdir /uploads

RUN chown -R cdli /uploads

WORKDIR /srv

CMD ["/usr/sbin/php-fpm7"]